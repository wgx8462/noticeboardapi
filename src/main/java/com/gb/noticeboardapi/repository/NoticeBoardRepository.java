package com.gb.noticeboardapi.repository;

import com.gb.noticeboardapi.entity.NoticeBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeBoardRepository extends JpaRepository<NoticeBoard, Long> {
}
