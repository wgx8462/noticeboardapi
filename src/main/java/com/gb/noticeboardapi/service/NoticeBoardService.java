package com.gb.noticeboardapi.service;

import com.gb.noticeboardapi.entity.NoticeBoard;
import com.gb.noticeboardapi.model.*;
import com.gb.noticeboardapi.repository.NoticeBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NoticeBoardService {
    private final NoticeBoardRepository noticeBoardRepository;

    public void setNoticeBoard(NoticeBoardRequest request) {
        NoticeBoard addData = new NoticeBoard();
        addData.setNoticeName(request.getNoticeName());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setCategorys(request.getCategorys());
        addData.setTodayDateTime(LocalDateTime.now(ZoneId.of("Asia/Seoul")));

        noticeBoardRepository.save(addData);
    }

    public List<NoticeBoardItem> getNoticeBoards() {
        List<NoticeBoard> originData = noticeBoardRepository.findAll();
        List<NoticeBoardItem> result = new LinkedList<>();

        for (NoticeBoard noticeBoard : originData) {
            NoticeBoardItem addItem = new NoticeBoardItem();
            addItem.setCategorysName(noticeBoard.getCategorys().getName());
            addItem.setNoticeName(noticeBoard.getNoticeName());
            addItem.setTodayDateTime(noticeBoard.getTodayDateTime());
            result.add(addItem);
        }
        return result;
    }

    public NoticeBoardResponse getNoticeBoard(long id) {
        NoticeBoard originData = noticeBoardRepository.findById(id).orElseThrow();
        NoticeBoardResponse response = new NoticeBoardResponse();

        response.setId(originData.getId());
        response.setCategorys(originData.getCategorys().getName());
        response.setNoticeName(originData.getNoticeName());
        response.setEtcMemo(originData.getEtcMemo());
        response.setTodayDateTime(originData.getTodayDateTime());

        return response;
    }

    public void putNoticeBoardNotice(long id, NoticeBoardNoticeChangeRequest request) {
        NoticeBoard originData = noticeBoardRepository.findById(id).orElseThrow();
        originData.setNoticeName(request.getNoticeName());

        noticeBoardRepository.save(originData);

    }

    public void putNoticeBoardEtc(long id, NoticeBoardEtcChangeRequest request) {
        NoticeBoard originData = noticeBoardRepository.findById(id).orElseThrow();
        originData.setEtcMemo(request.getEtcMemo());

        noticeBoardRepository.save(originData);
    }

    public void delNoticeBoard(long id) {
        noticeBoardRepository.deleteById(id);
    }


}
