package com.gb.noticeboardapi.entity;

import com.gb.noticeboardapi.enums.Categorys;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class NoticeBoard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String noticeName;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String etcMemo;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Categorys categorys;
    private LocalDateTime todayDateTime;
}
