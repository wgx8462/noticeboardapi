package com.gb.noticeboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeBoardNoticeChangeRequest {
    private String noticeName;
}
