package com.gb.noticeboardapi.model;

import com.gb.noticeboardapi.enums.Categorys;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeBoardRequest {
    @Column(nullable = false, length = 20)
    private String noticeName;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String etcMemo;
    @Enumerated(value = EnumType.STRING)
    private Categorys categorys;
}
