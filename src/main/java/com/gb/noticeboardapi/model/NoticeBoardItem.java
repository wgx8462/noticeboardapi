package com.gb.noticeboardapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
public class NoticeBoardItem {
    private String categorysName;
    private String noticeName;
    private LocalDateTime todayDateTime;
}
