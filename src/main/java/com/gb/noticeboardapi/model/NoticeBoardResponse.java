package com.gb.noticeboardapi.model;

import com.gb.noticeboardapi.enums.Categorys;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
public class NoticeBoardResponse {
    private Long id;
    private String noticeName;
    private String etcMemo;
    private String categorys;
    private LocalDateTime todayDateTime;
}
