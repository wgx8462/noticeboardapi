package com.gb.noticeboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeBoardEtcChangeRequest {
    private String etcMemo;
}
