package com.gb.noticeboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Categorys {
    GAME("게임"),
    MUSIC("음악"),
    HUMOR("유머");

    private String name;
}
