package com.gb.noticeboardapi.controller;

import com.gb.noticeboardapi.model.*;
import com.gb.noticeboardapi.service.NoticeBoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/notice-board")
public class NoticeBoardController {
    private final NoticeBoardService noticeBoardService;

    @PostMapping("/new")
    public String setNoticeBoard(@RequestBody NoticeBoardRequest request) {
        noticeBoardService.setNoticeBoard(request);

        return "Ok";
    }

    @GetMapping("/all")
    public List<NoticeBoardItem> getNoticeBoards() {
        return noticeBoardService.getNoticeBoards();
    }

    @GetMapping("/detail/{id}")
    public NoticeBoardResponse getNoticeBoard(@PathVariable long id) {
        return noticeBoardService.getNoticeBoard(id);
    }

    @PutMapping("/notice/{id}")
    public String putNoticeBoardNotice(@PathVariable long id, @RequestBody NoticeBoardNoticeChangeRequest request) {
        noticeBoardService.putNoticeBoardNotice(id, request);

        return "Ok";
    }

    @PutMapping("/etc/{id}")
    public String putNoticeBoardEtc(@PathVariable long id, @RequestBody NoticeBoardEtcChangeRequest request) {
        noticeBoardService.putNoticeBoardEtc(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delNoticeBoard(@PathVariable long id) {
        noticeBoardService.delNoticeBoard(id);

        return "OK";
    }
}
